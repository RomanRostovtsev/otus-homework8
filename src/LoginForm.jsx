import axios from 'axios';
import { useState } from 'react';

function LoginForm() {
    const [loggedIn, setLoggedIn] = useState(false);

    
    if (loggedIn) 
        return (
            <div>
                Logged in
            </div>
        );
    else 
        return (

            <div>
                <form onSubmit={(event) => {
                    
                    const data = {
                        "login": event.target.login.value,
                        "password": event.target.password.value
                    };
                    axios.post("/api/v1/Account/signin", data).then(response => {
                        setLoggedIn(true);
                    }).catch(error => {
                        let msg;
                        if (error.response) {
                            msg = error.response.status;
                        } else {
                            msg = error.message;
                        }
                        alert("Login failed.: " + msg);
                    });
                    event.preventDefault(); //to avoid form data sending

                }}>
                    Login: <input type="text" name="login"></input> <br></br>
                    Password: <input type="password" name="password"></input><br></br>
                    <input type="submit" value="Sign in"></input>
                </form>
            </div>
        );
    

}

export default LoginForm;